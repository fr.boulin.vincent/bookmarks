import React, { createContext } from 'react'
import PropTypes from 'prop-types'

import { useLocalStorage } from '../utils/useLocalStorage'

const sortByTitle = ({ title: a }, { title: b }) => a.localeCompare(b)

export const BookmarkContext = createContext()

// This context is using localStorage to store data
// In place of localStorage we can use a REST API to store and get datas
const BookmarkContextProvider = ({ children }) => {
  const [bookmarks, _setBookmarks] = useLocalStorage('bookmarks')
  const setBookmarks = bookmarks => _setBookmarks(bookmarks.sort(sortByTitle))

  const addBookmark = bookmark => {
    setBookmarks([...bookmarks, bookmark])
  }

  const updateBookmark = (index, bookmark) => {
    const temporary = bookmarks.filter((_, i) => i !== index)
    setBookmarks([...temporary, bookmark])
  }

  const deleteBookmark = index => {
    setBookmarks([...bookmarks.filter((_, i) => i !== index)])
  }

  return (
    <BookmarkContext.Provider
      value={{ bookmarks, addBookmark, updateBookmark, deleteBookmark }}
    >
      {children}
    </BookmarkContext.Provider>
  )
}

BookmarkContextProvider.propTypes = {
  children: PropTypes.node.isRequired
}

export default BookmarkContextProvider
