import { useState } from 'react'

const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(.*)?Z$/

const reviver = (_, value) => {
  if (typeof value === 'string' && dateFormat.test(value)) {
    return new Date(value)
  }

  return value
}

export const useLocalStorage = (key, defaultValue) => {
  const [storedValue, setStoredValue] = useState(() => {
    try {
      const data = window.localStorage.getItem(key)
      return data ? JSON.parse(data, reviver) : defaultValue
    } catch (error) {
      console.log(error)
      return defaultValue
    }
  })

  const setValue = value => {
    try {
      const data = value instanceof Function ? value(storedValue) : value
      setStoredValue(data)
      window.localStorage.setItem(key, JSON.stringify(data))
    } catch (error) {
      console.log(error)
    }
  }

  return [storedValue, setValue]
}
