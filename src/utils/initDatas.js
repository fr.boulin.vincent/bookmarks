const initBookmarks = () => {
  if (window.localStorage.getItem('bookmarks')) {
    return
  }

  window.localStorage.setItem(
    'bookmarks',
    JSON.stringify([
      {
        title: 'Big Buck Bunny',
        url: 'https://vimeo.com/1084537',
        author: 'Blender',
        width: 1280,
        height: 720,
        date: new Date(),
        tags: ['animals', 'rabbit', 'animation'],
        duration: 597
      },
      {
        title: 'Big_Buck_Bunny_movie_Screenshots',
        url: 'https://www.flickr.com/photos/smilingerin/4155117623/',
        author: 'Platform4',
        width: 600,
        height: 800,
        date: new Date(),
        tags: ['animals', 'rabbit']
      },
      {
        title: 'Disney Pixar - For the Birds',
        url: 'https://vimeo.com/61971077',
        author: 'Anekdotes',
        width: 640,
        height: 370,
        date: new Date(),
        tags: ['animals', 'birds', 'animation'],
        duration: 197
      }
    ])
  )
}

const initTags = () => {
  if (window.localStorage.getItem('tags')) {
    return
  }
  window.localStorage.setItem(
    'tags',
    JSON.stringify(['animals', 'birds', 'animation', 'rabbit'])
  )
}
const initDatas = () => {
  initTags()
  initBookmarks()
}

export default initDatas
