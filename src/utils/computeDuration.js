/**
 * Convert a duration in seconds to hours / minutes / seconds object
 * @param {number} duration
 * @return {object} duration in hours / minutes / seconds object
 */
const secondsToDuration = duration => {
  const hours = Math.floor(duration / (60 * 60))

  const divisorMinutes = duration % (60 * 60)
  const minutes = Math.floor(divisorMinutes / 60)

  const seconds = Math.ceil(divisorMinutes % 60)

  return { hours, minutes, seconds }
}

/**
 * Convert a duration of hours / minutes / seconds object into a duration in seconds
 * @param {object} duration
 * @param {number} duration.hours - hours
 * @param {number} duration.minutes - minutes
 * @param {number} duration.seconds - seconds
 * @return {number} duration in seconds
 */
const durationToSeconds = ({ hours, minutes, seconds }) =>
  hours * 60 * 60 + minutes * 60 + seconds

export { secondsToDuration, durationToSeconds }
