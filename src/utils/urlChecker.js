/**
 * Test if the url is from vimeo
 * @param {string} url
 * @return {boolean}
 */
const isVimeoUrl = url =>
  url ? url.match(/^https?:\/\/(www\.)?vimeo\.com.*/) !== null : false

/**
 * Test if the url is from flickr
 * @param {string} url
 * @return {boolean}
 */
const isFlickrUrl = url =>
  url ? url.match(/^https?:\/\/(www\.)?flickr\.com.*/) !== null : false

const isValidUrl = url => isFlickrUrl(url) || isVimeoUrl(url)

export { isVimeoUrl, isFlickrUrl, isValidUrl }
