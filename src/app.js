import { Header, Body } from './components'
import 'roboto-npm-webfont'
import BookmarkContextProvider from './contexts/bookmarkContext'
import initDatas from './utils/initDatas'

import './app.scss'

initDatas()

function App () {
  return (
    <div className='app'>
      <Header />
      <BookmarkContextProvider>
        <Body />
      </BookmarkContextProvider>
    </div>
  )
}

export default App
