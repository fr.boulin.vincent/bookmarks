import React from 'react'
import PropTypes from 'prop-types'
import { Button, TextField, Select, MenuItem } from '@material-ui/core'

import './pagination.scss'

const Pagination = ({ instance }) => {
  const {
    canPreviousPage,
    canNextPage,
    pageOptions,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: { pageIndex, pageSize }
  } = instance

  return (
    <div className='pagination'>
      <Button
        variant='outlined'
        color='primary'
        onClick={() => previousPage()}
        disabled={!canPreviousPage}
      >
        Previous
      </Button>

      <TextField
        className='pageNumber'
        type='number'
        value={pageIndex + 1}
        onChange={e => {
          const page = e.target.value ? Number(e.target.value) - 1 : 0
          gotoPage(page)
        }}
        min='1'
        max={pageOptions.length}
        disabled={!canPreviousPage && !canNextPage}
      />
      <span> of {pageOptions.length}</span>
      <Button
        variant='outlined'
        color='primary'
        onClick={() => nextPage()}
        disabled={!canNextPage}
      >
        Next
      </Button>
      <Select
        value={pageSize}
        onChange={e => {
          setPageSize(Number(e.target.value))
        }}
      >
        {[1, 5, 10, 20, 30, 40, 50].map(pageSize => (
          <MenuItem key={pageSize} value={pageSize}>
            Show {pageSize}
          </MenuItem>
        ))}
      </Select>
    </div>
  )
}

Pagination.propTypes = {
  instance: PropTypes.shape({
    canPreviousPage: PropTypes.bool,
    canNextPage: PropTypes.bool,
    pageOptions: PropTypes.array,
    gotoPage: PropTypes.func,
    nextPage: PropTypes.func,
    previousPage: PropTypes.func,
    setPageSize: PropTypes.func
  })
}

export default Pagination
