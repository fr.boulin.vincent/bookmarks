import React from 'react'
import { TextField } from '@material-ui/core'

import './globalFilter.scss'

const GlobalFilter = ({ instance }) => {
  const {
    data,
    preGlobalFilteredRows,
    globalFilteredRows,
    setGlobalFilter,
    state: { globalFilter }
  } = instance

  return (
    <div className='globalFilter'>
      <TextField
        id='search'
        label='Search'
        variant='outlined'
        value={globalFilter || ''}
        onChange={event => setGlobalFilter(event.target.value)}
        size='small'
      />
      <span className='count'>{`${globalFilteredRows?.length ||
        data.length} / ${preGlobalFilteredRows?.length || data.length}`}</span>
    </div>
  )
}

export default GlobalFilter
