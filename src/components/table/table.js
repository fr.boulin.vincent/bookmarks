import React from 'react'
import {
  Table as MuiTable,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Button
} from '@material-ui/core'
import {
  KeyboardArrowUp,
  KeyboardArrowDown,
  Add as AddIcon
} from '@material-ui/icons'
import {
  useTable,
  usePagination,
  useSortBy,
  useGlobalFilter
} from 'react-table'

import Pagination from './pagination'
import GlobalFilter from './globalFilter'
import { fuzzyTextFilter } from './filters'

import './table.scss'

const tableHooks = [useGlobalFilter, useSortBy, usePagination]

const Table = ({ columns, data, onAdd }) => {
  const instance = useTable(
    { columns, data, filterTypes: { fuzzyTextFilter } },
    ...tableHooks
  )

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page
  } = instance

  return (
    <div className='table'>
      <div className='header'>
        {onAdd && (
          <Button variant='outlined' color='primary' onClick={onAdd}>
            Add
            <AddIcon />
          </Button>
        )}
        <GlobalFilter instance={instance} />
      </div>
      <MuiTable {...getTableProps()} size='small' table-layout='fixed'>
        <TableHead>
          {headerGroups.map(headerGroup => (
            <TableRow {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <TableCell
                  {...column.getHeaderProps(column.getSortByToggleProps())}
                >
                  <span className='thContent'>
                    {column.render('Header')}
                    {column.isSorted ? (
                      column.isSortedDesc ? (
                        <KeyboardArrowDown />
                      ) : (
                        <KeyboardArrowUp />
                      )
                    ) : (
                      ''
                    )}
                  </span>
                </TableCell>
              ))}
            </TableRow>
          ))}
        </TableHead>
        <TableBody {...getTableBodyProps()}>
          {page.map((row, i) => {
            prepareRow(row)
            return (
              <TableRow {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return (
                    <TableCell
                      {...cell.getCellProps()}
                      style={{
                        maxWidth: cell.column.maxWidth,
                        paddingLeft: cell.column.paddingLeft,
                        paddingRight: cell.column.paddingRight
                      }}
                    >
                      {cell.render('Cell')}
                    </TableCell>
                  )
                })}
              </TableRow>
            )
          })}
        </TableBody>
      </MuiTable>
      <Pagination instance={instance} />
    </div>
  )
}

export default Table
