import React, { useState } from 'react'
import { TextField, Button, Chip } from '@material-ui/core'

import { useLocalStorage } from '../../utils/useLocalStorage'

import './tagsManager.scss'

const TagsManager = ({ selectedTags: selectedTagsProps, onChange }) => {
  const [tags, setTags] = useLocalStorage('tags', [])
  const [selectedTags, _setSelectedTags] = useState(selectedTagsProps || [])
  const setSelectedTags = newTags => {
    _setSelectedTags(newTags)
    if (onChange) {
      onChange(newTags)
    }
  }
  const [tag, setTag] = useState('')

  const availableTags = tags.filter(tag => !selectedTags.includes(tag))

  const removeElemFromArray = elem => (_, index, array) =>
    array.indexOf(elem) !== index

  const onDeleteTag = tag => setTags(tags.filter(removeElemFromArray(tag)))

  const onDeleteSelectedTag = tag =>
    setSelectedTags(selectedTags.filter(removeElemFromArray(tag)))

  const onClickTag = tag => {
    setSelectedTags([...selectedTags, tag])
  }

  return (
    <div className='tagsManager'>
      <div className='addTagContainer'>
        <TextField
          label='Add tag'
          className='addTagInput'
          defaultValue={tag}
          onChange={event => setTag(event.target.value)}
        />
        <Button
          id='addTagButton'
          onClick={() => {
            setTags([...tags, tag])
            setSelectedTags([...selectedTags, tag])
            setTag('')
          }}
          color='primary'
          variant='outlined'
          disabled={!tag}
        >
          Add
        </Button>
      </div>
      <div className='tagSelector'>
        <div className='availableTags tagContent'>
          <span>Available tags</span>
          {availableTags.map((tag, index) => (
            <Chip
              className='tag'
              key={index}
              label={tag}
              onDelete={() => onDeleteTag(tag)}
              onClick={() => onClickTag(tag)}
            />
          ))}
        </div>
        <div className='selectedTags tagContent'>
          <span>Selected tags</span>
          {selectedTags.map((tag, index) => (
            <Chip
              className='tag'
              key={index}
              label={tag}
              onDelete={() => onDeleteSelectedTag(tag)}
              color='primary'
            />
          ))}
        </div>
      </div>
    </div>
  )
}

export default TagsManager
