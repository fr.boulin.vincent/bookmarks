import React, { useState, useMemo, useContext, useCallback } from 'react'
import { Dialog, DialogContent, DialogTitle, Chip } from '@material-ui/core'
import { Edit as EditIcon, Delete as DeleteIcon } from '@material-ui/icons'

import Table from '../table/table'
import BookmarkForm from './bookmarkForm'
import { BookmarkContext } from '../../contexts/bookmarkContext'
import { secondsToDuration } from '../../utils/computeDuration'
import { isVimeoUrl } from '../../utils/urlChecker'

import './body.scss'

const Body = () => {
  const { bookmarks, deleteBookmark } = useContext(BookmarkContext)
  const [open, setOpen] = useState(false)
  const [indexEdited, setIndexEdited] = useState(-1)
  const [bookmarkEdited, setBookmarkEdited] = useState({})

  const edit = useCallback(
    (index, bookmark) => {
      setIndexEdited(index)
      setBookmarkEdited(bookmark)
      openForm()
    },
    [setIndexEdited, setBookmarkEdited]
  )

  const openForm = () => setOpen(true)
  const closeForm = () => {
    setIndexEdited(-1)
    setBookmarkEdited({})
    setOpen(false)
  }

  const columns = useMemo(
    () => [
      {
        id: 'actions',
        maxWidth: 50,
        paddingLeft: 0,
        paddingRight: 0,
        Cell: ({ row: { index, original: bookmark } }) => {
          return (
            <div className='actions'>
              <button onClick={() => edit(index, bookmark)}>
                <EditIcon></EditIcon>
              </button>
              <button onClick={() => deleteBookmark(index)}>
                <DeleteIcon></DeleteIcon>
              </button>
            </div>
          )
        }
      },
      {
        id: 'icon',
        accessor: 'url',
        Cell: props => {
          return isVimeoUrl(props.value) ? (
            <img src='./vimeo.png' alt='vimeo' width='30px' />
          ) : (
            <img src='./flickr.png' alt='flickr' width='30px' />
          )
        }
      },
      { Header: 'Title', accessor: 'title', maxWidth: 100 },
      {
        Header: 'Url',
        accessor: 'url',
        maxWidth: 150,
        Cell: props => (
          <a className='url' href={props.value}>
            {props.value}
          </a>
        )
      },
      { Header: 'Author', accessor: 'author' },
      {
        Header: 'Added date',
        accessor: 'date',
        Cell: props => <div>{props.value?.toLocaleDateString()}</div>
      },
      {
        Header: 'Width',
        accessor: 'width',
        Cell: ({ value }) => (value ? `${value} px` : '')
      },
      {
        Header: 'Height',
        accessor: 'height',
        Cell: ({ value }) => (value ? `${value} px` : '')
      },
      {
        Header: 'Duration',
        accessor: 'duration',
        Cell: props => {
          const { hours, minutes, seconds } = secondsToDuration(props.value)
          return props.value
            ? `${hours.toString().padStart(2, '0')}h
            ${minutes.toString().padStart(2, '0')}m ${seconds}s`
            : ''
        }
      },
      {
        Header: 'Tags',
        accessor: 'tags',
        Cell: props => {
          return (
            <>
              {props.value?.map((elem, index) => (
                <Chip
                  color='primary'
                  key={index}
                  label={elem}
                  className='tag'
                />
              ))}
            </>
          )
        }
      }
    ],
    [edit, deleteBookmark]
  )

  if (!bookmarks) {
    return <div className='body'></div>
  }

  return (
    <div className='body'>
      <Dialog onClose={closeForm} open={open}>
        <DialogTitle>{`${
          indexEdited >= 0 ? 'Edit' : 'Add'
        } boomark`}</DialogTitle>
        <DialogContent>
          <BookmarkForm
            index={indexEdited}
            bookmark={bookmarkEdited}
            onCancel={closeForm}
            onSubmit={closeForm}
          />
        </DialogContent>
      </Dialog>
      <Table columns={columns} data={bookmarks} onAdd={() => setOpen(true)} />
    </div>
  )
}

export default Body
