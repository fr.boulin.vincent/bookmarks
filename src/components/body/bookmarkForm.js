import React, { useState, useEffect, useContext } from 'react'
import { TextField, Button } from '@material-ui/core'
import TagsManager from './tagsManager'
import { BookmarkContext } from '../../contexts/bookmarkContext'
import { isValidUrl, isVimeoUrl } from '../../utils/urlChecker'
import {
  secondsToDuration,
  durationToSeconds
} from '../../utils/computeDuration'

import './bookmarkForm.scss'

const BookmarkForm = ({
  index,
  bookmark: bookmarkProp,
  onSubmit: onSubmitProp,
  onCancel: onCancelProp
}) => {
  const [bookmark, setBookmark] = useState(bookmarkProp || {})
  const [isVideo, setIsVideo] = useState(false)
  const [error, setError] = useState('')

  const { addBookmark, updateBookmark } = useContext(BookmarkContext)

  useEffect(() => {
    if (isVimeoUrl(bookmark?.url)) {
      setIsVideo(true)
    } else {
      setIsVideo(false)
    }
  }, [bookmark, setIsVideo])

  const validUrl = () => {
    setError('')
    if (!isValidUrl(bookmark.url)) {
      setError('The url is not in vimeo or flicrk format')
    }
  }

  const getDisplayedDuration = duration => {
    const { hours, minutes, seconds } = secondsToDuration(duration)
    return `${hours.toString().padStart(2, '0')}:${minutes
      .toString()
      .padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`
  }

  const convertDurationFromInput = duration => {
    const parts = duration?.split(':').map(elem => parseInt(elem))
    const [hours, minutes, seconds = 0] = parts
    return durationToSeconds({ hours, minutes, seconds })
  }

  const onSubmit = bookmark => {
    const tmp = { date: new Date(), ...bookmark }
    if (index >= 0) {
      updateBookmark(index, tmp)
    } else {
      addBookmark(tmp)
    }
    if (onSubmitProp) {
      onSubmitProp(tmp)
    }
  }

  const onCancel = () => {
    if (onCancelProp) {
      onCancelProp()
    }
  }

  const updateTags = tags => {
    setBookmark({ ...bookmark, ...{ tags } })
  }

  return (
    <div className='bookmarkForm'>
      <TextField
        label='Title *'
        defaultValue={bookmark?.title || ''}
        fullWidth={true}
        onChange={event =>
          setBookmark({ ...bookmark, ...{ title: event.target.value } })
        }
      />
      <TextField
        label='Url *'
        defaultValue={bookmark?.url}
        fullWidth={true}
        onChange={event =>
          setBookmark({ ...bookmark, ...{ url: event.target.value } })
        }
        onBlur={validUrl}
      />
      <TextField
        label='Author'
        defaultValue={bookmark?.author}
        fullWidth={true}
        onChange={event =>
          setBookmark({ ...bookmark, ...{ author: event.target.value } })
        }
      />
      <TextField
        label='Width (px)'
        defaultValue={bookmark?.width}
        fullWidth={true}
        type='number'
        min='0'
        onChange={event =>
          setBookmark({ ...bookmark, ...{ width: event.target.value } })
        }
      />
      <TextField
        label='Height (px)'
        defaultValue={bookmark?.height}
        fullWidth={true}
        type='number'
        min='0'
        onChange={event =>
          setBookmark({ ...bookmark, ...{ height: event.target.value } })
        }
      />
      {isVideo && (
        <div className='MuiInputBase-root timePicker'>
          <label htmlFor='time' className='MuiFormLabel-root'>
            Duration
          </label>
          <input
            className='MuiInputBase-input'
            name='time'
            type='time'
            step='1'
            defaultValue={getDisplayedDuration(bookmark?.duration || 0)}
            onChange={event => {
              console.log(event.target.value)
              setBookmark({
                ...bookmark,
                ...{ duration: convertDurationFromInput(event.target.value) }
              })
            }}
          />
        </div>
      )}
      <TagsManager selectedTags={bookmark.tags} onChange={updateTags} />
      <div className='tips'>* The title and url fields are mandatory</div>
      {error && <div className='error'>{error}</div>}
      <div className='actions'>
        <Button color='secondary' variant='outlined' onClick={onCancel}>
          Cancel
        </Button>
        <Button
          color='primary'
          variant='contained'
          onClick={() => onSubmit(bookmark)}
          disabled={
            !bookmark?.url || !bookmark.title || !isValidUrl(bookmark.url)
          }
        >
          Submit
        </Button>
      </div>
    </div>
  )
}

export default BookmarkForm
